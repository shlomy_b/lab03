//Shlomo Bensimhon
//1837702
package linearAlgebra;

public class Vector3d {
	
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		return (Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));  
	}
	
	public double dotProduct(Vector3d vec1){
		double newX = (vec1.getX() * this.x);
		double newY = (vec1.getY() * this.y);
		double newZ = (vec1.getZ() * this.z);
		return (newX + newY + newZ);
	}
	
	public Vector3d add(Vector3d newVec){
		double newX = (newVec.getX() + this.x);
		double newY = (newVec.getY() + this.y);
		double newZ = (newVec.getZ() + this.z);
		return new Vector3d(newX, newY, newZ);
	}
}










