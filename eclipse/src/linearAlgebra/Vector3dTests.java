//Shlomo Benismhon
//1837702

package linearAlgebra;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	public void getterTest() {
		Vector3d v1 = new Vector3d(2.0, 4.0, 6.0);
		assertEquals(2.0, v1.getX());
		assertEquals(4.0, v1.getY());
		assertEquals(6.0, v1.getZ());
	}
	
	@Test
	public void magnitudeTest() {
		Vector3d v2 = new Vector3d(2, 4, 6);
		assertEquals(7.483314773547883, v2.magnitude());
	}
	
	@Test
	public void dotProductTest() {
		Vector3d v3 = new Vector3d(2, 4, 6);
		Vector3d v4 = new Vector3d(8, 10, 12);
		assertEquals(128, v3.dotProduct(v4));
	}
	
	@Test
	public void addTest() {
		Vector3d v5 = new Vector3d(14, 16, 18);
		Vector3d v6 = new Vector3d(20, 22, 24);
		assertEquals(34, v5.add(v6).getX());
		assertEquals(38, v5.add(v6).getY());
		assertEquals(42, v5.add(v6).getZ());
	}
}









